#!/bin/bash
#
# Detects repository VCS and begins VCS-specific processing
#
#  Copyright (C) 2012  Mike Gerwitz
#
#  This file is part of repo2html.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #

_stderrout()
{
  prefix="$1"
  sed 's/^/['"$prefix"'] /g' >&2
}

_rssout()
{
  _stderrout 'RSS'
}
_htmlout()
{
  _stderrout 'HTML'
}
_reout()
{
  _htmlout 2> >( _stderrout 'Reprocessing' )
}


# repository detection
repotype=
if repodir="$( git rev-parse --git-dir 2>/dev/null )"; then
  # use absolute path for git directory
  export GIT_DIR="$( cd $repodir && pwd )"
  repotype=git
else
  # contribute!
  echo "fatal: only git repositories are currently supported" >&2
  exit 128
fi

# store our cwd and enter our own (to make invoking relative scripts easier)
export path_root="$( pwd )"
path_out="$path_root"
path="$( dirname "$0" )"
cd "$path"

# provides configuration warning and outputs a default
cwarn()
{
  echo "Warning: no $1 provided; defaulting to: ${2:-(empty)}" >&2
  echo "$2"
}

# these configuration opts must be read first since they will be used to load
# configuration options that must be overridden by future command-line options
path_tpl=
while true; do
  getopts :T: opt
  [ -z "$OPTARG" ] && break

  case "$opt" in
    T) path_tpl="$OPTARG"; break;;
    ?) ((OPTIND++));;
  esac
done
OPTIND=0

source include/tpl "$path_tpl"

# if a custom template path was given, attempt to load the configuration
cfgpath="$path_tpl/.config"
[ -n "$path_tpl" -a -e "$cfgpath" ] && {
  echo "loading template configuration $cfgpath..." >&2
  source "$cfgpath"
}

# some initial defaults
export html_ext="${html_ext-.html}"
export html_link_ext="${html_link_ext-.html}"
do_raw=
raw_tpl=

# configuration (note that this setup in conjunction with the below defaults
# imply that they can be passed in as environment variables as an alternative to
# options)
while getopts t:d:c:e:l:f:F:i:o:u:C:E:R:T:U: opt; do
  case "$opt" in
    t) title="$OPTARG";;
    d) desc="$OPTARG";;
    c) copyright="$OPTARG";;
    e) html_ext="$OPTARG";;
    l) license="$OPTARG";;
    f) msgfmt="$path_root/$OPTARG";;
    F) listfilter="$path_root/$OPTARG";;
    i)
      do_raw=1
      raw_tpl="$OPTARG"
      ;;
    o)
      # recognize both absolute and relative paths
      if [ "${OPTARG:0:1}" == / ]; then
        path_out="$OPTARG"
      else
        path_out="$path_out/$OPTARG"
      fi
      ;;
    u) repo_url="$OPTARG";;
    U) repo_commit_url="$OPTARG";;
    C) html_external_css="$OPTARG";;
    E) html_link_ext="$OPTARG";;
    R) rss_count="$OPTARG";;
    T) ;; #already handled
    ?) exit 64;;
  esac
done

shift $((OPTIND-1))

url_root="${1}"
test "$do_raw" -o "$url_root" || { echo 'Missing root URL' >&2; exit 64; }

# default title to repository name (as taken from the directory)
title="${title:-$( cwarn title $( basename "$path_out" ) )}"
desc="${desc:-$( cwarn description )}"

# default to a restrictive copyright (unfortunately, that's the best we can do,
# since copyright is restrictive by default)
copyright="${copyright:-$( cwarn copyright 'Respective Authors' )}"
license="${license:-$( cwarn license )}"

# provide default message formatter if necessary
export msgfmt_default=./msgfmt
msgfmt="${msgfmt:-$default}"

# make configuration available to all scripts
export title desc copyright license msgfmt url_root path_tpl path \
  html_external_css html_footer html_index_footer html_commit_footer \
  html_pre_index html_body_class html_tsep html_head repo_url \
  repo_commit_url

# if raw processing was requested, then we are done; pass the torch
test $do_raw && {
  # if requested template does not exist in template dir, fall back to the
  # default template dir
  tpl="$path_tpl/$raw_tpl"
  [ -x "$tpl" ] || tpl="$path_tpl_default/$raw_tpl"

  exec ./raw "$tpl" "$msgfmt"
}

# clear the cachefile (TODO: we could maintain cache files from previous runs if
# we offer a flag that opts out of reprocessing previously processed commits)
echo "Clearing hashcache..." >&2
./hashcache clear

# set the cref error log, which will allow us to re-process *only* those commits
# that actually need to be reprocessed; this avoids the need to prime the
# hashcache, saving cycles
cref_errlog_first="$path_root/.cref-errlog"
export cref_errlog="$cref_errlog_first"
>"$cref_errlog_first"

# pass commit list to the HTML and RSS processors (cache list to disk for
# reference and further processing); note that we clear the cref_errlog for the
# RSS feed so that we do not get duplicates
listcache="$path_root/.clist"
"$repotype"/list \
  | grep -vf "${listfilter:-/dev/null}" \
  | tee \
    >( cref_errlog= ./rss "$repotype" "$url_root" "$rss_count" \
       > "$path_out/rss.xml" \
       2> >( _rssout )
   ) \
    "$listcache" \
    | ./processor "$repotype" "$path_out" 2> >( _htmlout )

# re-process cref errors (but only once; any errors at this point will be
# considered to be problem refs) TODO: RSS; note that we redirect all output to
# /dev/null so as not to re-output the index template
export cref_errlog="$path_root/.cref-bad"
>"$cref_errlog"
( grep -f<( sed 's/^/\^/' "$cref_errlog_first" ) "$listcache" \
  | ./processor "$repotype" "$path_out" 2> >( _reout ) \
) >/dev/null

# wait for all processes to finish
wait

# if any invalid crefs remain, then they're bad
[ ! -s "$cref_errlog" ] || {
  echo "warning: bad cref(s); see $cref_errlog" >&2
}
