#!/bin/bash
#
# Index template
#
#  Copyright (C) 2013  Mike Gerwitz
#
#  This file is part of repo2html.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #

output-heading()
{
  # 1 = dategroup
  # we assign the heading to the id for the purpose of URL hashes
  echo "<h3 id="$1" class="index">$1</h3>"

  # every heading is followed by at least one index line
  echo '<ul class="index">'
}

output-line()
{
  # 1 = day, 2 = pagefile, 3 = fmtsubject
  printf '<li><span class="day">(%s)</span> <a href="%s">%s</a></li>' "$@"
}

close-heading()
{
  echo '</ul>'
}

cat <<EOH
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <link rel="alternate" title="RSS Feed"
      href="rss.xml" type="application/rss+xml" />
    $( [ -n "$html_external_css" ] \
      && printf '<link rel="stylesheet" type="text/css" href="%s" />' \
        "$html_external_css"
    )
    <title>$title${subject:+"${html_tsep:-&mdash;}$subject"}</title>
    $html_head
  </head>
  <body class="${html_body_class:-index}">
    <header>
      <h1 class="title"><a href="${url_root:-/}">$title</a></h1>
      <h2 class="desc">$desc</h2>
    </header>
    $html_pre_index
    <div class="content">
EOH

# generate index and populate statistical vars
type do-index &>/dev/null \
  && do-index output-heading output-line close-heading \
  || {
    # no do-index is defined; default to accepting input from stdin
    yearrange="$( date +%Y )"
    lastts="$( date +%s )"

    [ -n "$subject" ] && printf '<h3 class="subject">%s</h3>\n' "$subject"
    cat
  }

cat <<EOF
    </div>
    <footer>
      <div id="rss"><a href="rss.xml">RSS</a></div>
      <hr />
      $( [ -n "$html_footer" ] \
        && printf '<div class="pre-copyright">%s</div>' "$html_footer"
      )
      $( [ -n "$html_index_footer" ] \
        && printf '<div class="pre-copyright">%s</div>' "$html_index_footer"
      )
      <div id="copyright">
        Copyright &copy; $yearrange $copyright.
        $license
      </div>
      <div>Last Updated: $( ./tsdate "$lastts" '%F %H:%M:%S' )</div>
      $( [ -n "$commit-id" ] \
        && printf '<div class="commit-id">%s</div>' "$lasthash"
      )
    </footer>
  </body>
</html>
EOF
